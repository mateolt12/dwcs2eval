<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190201200111 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE usuario (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, nombre VARCHAR(255) NOT NULL, apellido1 VARCHAR(255) NOT NULL, apellido2 VARCHAR(255) NOT NULL, calle VARCHAR(255) NOT NULL, numero INT DEFAULT NULL, provincia VARCHAR(255) NOT NULL, localidad VARCHAR(255) NOT NULL, codigo_postal INT NOT NULL, telefono VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_2265B05DF85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario_alerta_informacion (usuario_id INT NOT NULL, alerta_informacion_id INT NOT NULL, INDEX IDX_86396E58DB38439E (usuario_id), INDEX IDX_86396E58DFC93304 (alerta_informacion_id), PRIMARY KEY(usuario_id, alerta_informacion_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aviso (id INT AUTO_INCREMENT NOT NULL, latitud DOUBLE PRECISION NOT NULL, longitud DOUBLE PRECISION NOT NULL, ip VARCHAR(255) NOT NULL, foto VARCHAR(255) NOT NULL, fecha DATETIME NOT NULL, comentario VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE alerta_informacion (id INT AUTO_INCREMENT NOT NULL, latitud DOUBLE PRECISION NOT NULL, longitud DOUBLE PRECISION NOT NULL, radio_km DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE alerta_informacion_aviso (alerta_informacion_id INT NOT NULL, aviso_id INT NOT NULL, INDEX IDX_BAE21CB3DFC93304 (alerta_informacion_id), INDEX IDX_BAE21CB39D17A5A7 (aviso_id), PRIMARY KEY(alerta_informacion_id, aviso_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voto (id INT AUTO_INCREMENT NOT NULL, aviso_id INT NOT NULL, confirmado TINYINT(1) NOT NULL, ip VARCHAR(255) NOT NULL, fecha DATETIME NOT NULL, comentario VARCHAR(255) DEFAULT NULL, INDEX IDX_BAC56C7A9D17A5A7 (aviso_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE usuario_alerta_informacion ADD CONSTRAINT FK_86396E58DB38439E FOREIGN KEY (usuario_id) REFERENCES usuario (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE usuario_alerta_informacion ADD CONSTRAINT FK_86396E58DFC93304 FOREIGN KEY (alerta_informacion_id) REFERENCES alerta_informacion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE alerta_informacion_aviso ADD CONSTRAINT FK_BAE21CB3DFC93304 FOREIGN KEY (alerta_informacion_id) REFERENCES alerta_informacion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE alerta_informacion_aviso ADD CONSTRAINT FK_BAE21CB39D17A5A7 FOREIGN KEY (aviso_id) REFERENCES aviso (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE voto ADD CONSTRAINT FK_BAC56C7A9D17A5A7 FOREIGN KEY (aviso_id) REFERENCES aviso (id)');
        $this->addSql('ALTER TABLE modificacion ADD CONSTRAINT FK_22AA4AF4DB38439E FOREIGN KEY (usuario_id) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE modificacion ADD CONSTRAINT FK_22AA4AF49D17A5A7 FOREIGN KEY (aviso_id) REFERENCES aviso (id)');
        $this->addSql('ALTER TABLE modificacion ADD CONSTRAINT FK_22AA4AF41D613A98 FOREIGN KEY (voto_id) REFERENCES voto (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE modificacion DROP FOREIGN KEY FK_22AA4AF4DB38439E');
        $this->addSql('ALTER TABLE usuario_alerta_informacion DROP FOREIGN KEY FK_86396E58DB38439E');
        $this->addSql('ALTER TABLE modificacion DROP FOREIGN KEY FK_22AA4AF49D17A5A7');
        $this->addSql('ALTER TABLE alerta_informacion_aviso DROP FOREIGN KEY FK_BAE21CB39D17A5A7');
        $this->addSql('ALTER TABLE voto DROP FOREIGN KEY FK_BAC56C7A9D17A5A7');
        $this->addSql('ALTER TABLE usuario_alerta_informacion DROP FOREIGN KEY FK_86396E58DFC93304');
        $this->addSql('ALTER TABLE alerta_informacion_aviso DROP FOREIGN KEY FK_BAE21CB3DFC93304');
        $this->addSql('ALTER TABLE modificacion DROP FOREIGN KEY FK_22AA4AF41D613A98');
        $this->addSql('DROP TABLE usuario');
        $this->addSql('DROP TABLE usuario_alerta_informacion');
        $this->addSql('DROP TABLE aviso');
        $this->addSql('DROP TABLE alerta_informacion');
        $this->addSql('DROP TABLE alerta_informacion_aviso');
        $this->addSql('DROP TABLE voto');
    }
}
