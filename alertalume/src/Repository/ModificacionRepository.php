<?php

namespace App\Repository;

use App\Entity\Modificacion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Modificacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Modificacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Modificacion[]    findAll()
 * @method Modificacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModificacionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Modificacion::class);
    }

    // /**
    //  * @return Modificacion[] Returns an array of Modificacion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Modificacion
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
