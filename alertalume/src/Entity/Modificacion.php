<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ModificacionRepository")
 */
class Modificacion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tipo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="modificaciones")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Aviso", inversedBy="modificaciones")
     */
    private $aviso;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Voto", inversedBy="modificaciones")
     */
    private $voto;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    public function setUsuario(?Usuario $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getAviso(): ?Aviso
    {
        return $this->aviso;
    }

    public function setAviso(?Aviso $aviso): self
    {
        $this->aviso = $aviso;

        return $this;
    }

    public function getVoto(): ?Voto
    {
        return $this->voto;
    }

    public function setVoto(?Voto $voto): self
    {
        $this->voto = $voto;

        return $this;
    }
}
