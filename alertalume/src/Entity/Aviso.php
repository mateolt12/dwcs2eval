<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvisoRepository")
 */
class Aviso
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="float")
     */
    private $latitud;

    /**
     * @ORM\Column(type="float")
     */
    private $longitud;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $foto;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comentario;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AlertaInformacion", mappedBy="avisos")
     */
    private $alertasInformacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Voto", mappedBy="aviso", orphanRemoval=true)
     */
    private $votos;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Modificacion", mappedBy="aviso")
     */
    private $modificaciones;

    public function __construct()
    {
        $this->alertasInformacion = new ArrayCollection();
        $this->votos = new ArrayCollection();
        $this->modificaciones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLatitud(): ?float
    {
        return $this->latitud;
    }

    public function setLatitud(float $latitud): self
    {
        $this->latitud = $latitud;

        return $this;
    }

    public function getLongitud(): ?float
    {
        return $this->longitud;
    }

    public function setLongitud(float $longitud): self
    {
        $this->longitud = $longitud;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getFoto(): ?string
    {
        return $this->foto;
    }

    public function setFoto(string $foto): self
    {
        $this->foto = $foto;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getComentario(): ?string
    {
        return $this->comentario;
    }

    public function setComentario(?string $comentario): self
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * @return Collection|AlertaInformacion[]
     */
    public function getAlertasInformacion(): Collection
    {
        return $this->alertasInformacion;
    }

    public function addAlertasInformacion(AlertaInformacion $alertasInformacion): self
    {
        if (!$this->alertasInformacion->contains($alertasInformacion)) {
            $this->alertasInformacion[] = $alertasInformacion;
            $alertasInformacion->addAviso($this);
        }

        return $this;
    }

    public function removeAlertasInformacion(AlertaInformacion $alertasInformacion): self
    {
        if ($this->alertasInformacion->contains($alertasInformacion)) {
            $this->alertasInformacion->removeElement($alertasInformacion);
            $alertasInformacion->removeAviso($this);
        }

        return $this;
    }

    /**
     * @return Collection|Voto[]
     */
    public function getVotos(): Collection
    {
        return $this->votos;
    }

    public function addVoto(Voto $voto): self
    {
        if (!$this->votos->contains($voto)) {
            $this->votos[] = $voto;
            $voto->setAviso($this);
        }

        return $this;
    }

    public function removeVoto(Voto $voto): self
    {
        if ($this->votos->contains($voto)) {
            $this->votos->removeElement($voto);
            // set the owning side to null (unless already changed)
            if ($voto->getAviso() === $this) {
                $voto->setAviso(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Modificacion[]
     */
    public function getModificaciones(): Collection
    {
        return $this->modificaciones;
    }

    public function addModificacione(Modificacion $modificacione): self
    {
        if (!$this->modificaciones->contains($modificacione)) {
            $this->modificaciones[] = $modificacione;
            $modificacione->setAviso($this);
        }

        return $this;
    }

    public function removeModificacione(Modificacion $modificacione): self
    {
        if ($this->modificaciones->contains($modificacione)) {
            $this->modificaciones->removeElement($modificacione);
            // set the owning side to null (unless already changed)
            if ($modificacione->getAviso() === $this) {
                $modificacione->setAviso(null);
            }
        }

        return $this;
    }
}
