<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VotoRepository")
 */
class Voto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $confirmado;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ip;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comentario;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Aviso", inversedBy="votos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $aviso;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Modificacion", mappedBy="voto")
     */
    private $modificaciones;

    public function __construct()
    {
        $this->modificaciones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConfirmado(): ?bool
    {
        return $this->confirmado;
    }

    public function setConfirmado(bool $confirmado): self
    {
        $this->confirmado = $confirmado;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getComentario(): ?string
    {
        return $this->comentario;
    }

    public function setComentario(?string $comentario): self
    {
        $this->comentario = $comentario;

        return $this;
    }

    public function getAviso(): ?Aviso
    {
        return $this->aviso;
    }

    public function setAviso(?Aviso $aviso): self
    {
        $this->aviso = $aviso;

        return $this;
    }

    /**
     * @return Collection|Modificacion[]
     */
    public function getModificaciones(): Collection
    {
        return $this->modificaciones;
    }

    public function addModificacione(Modificacion $modificacione): self
    {
        if (!$this->modificaciones->contains($modificacione)) {
            $this->modificaciones[] = $modificacione;
            $modificacione->setVoto($this);
        }

        return $this;
    }

    public function removeModificacione(Modificacion $modificacione): self
    {
        if ($this->modificaciones->contains($modificacione)) {
            $this->modificaciones->removeElement($modificacione);
            // set the owning side to null (unless already changed)
            if ($modificacione->getVoto() === $this) {
                $modificacione->setVoto(null);
            }
        }

        return $this;
    }
}
