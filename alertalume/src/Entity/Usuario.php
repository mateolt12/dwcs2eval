<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 */
class Usuario implements UserInterface, \Serializable {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=180, unique=true)
	 */
	private $username;

	/**
	 * @ORM\Column(type="string")
	 */
	private $roles = [];

	/**
	 * @var string The hashed password
	 * @ORM\Column(type="string")
	 */
	private $password;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $nombre;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $apellido1;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $apellido2;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $calle;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $numero;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $provincia;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $localidad;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $codigoPostal;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $telefono;

	/**
	 * @ORM\ManyToMany(targetEntity="App\Entity\AlertaInformacion", inversedBy="usuarios")
	 */
	private $alertas;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Modificacion", mappedBy="usuario", orphanRemoval=true)
	 */
	private $modificaciones;

	public function __construct() {
		$this->alertas = new ArrayCollection();
		$this->modificaciones = new ArrayCollection();
	}

	public function getId():  ? int {
		return $this->id;
	}

	/**
	 * A visual identifier that represents this user.
	 *
	 * @see UserInterface
	 */
	public function getUsername() : string {
		return (string) $this->username;
	}

	public function setUsername(string $username): self{
		$this->username = $username;

		return $this;
	}

	/**
	 * @see UserInterface
	 */
	public function getRoles(): array
	{
		$roles = $this->roles;
		// guarantee every user at least has ROLE_USER
		$roles[] = 'ROLE_USER';

		return array_unique($roles);
	}

	public function setRoles(array $roles): self{
		$this->roles = $roles;

		return $this;
	}

	/**
	 * @see UserInterface
	 */
	public function getPassword(): string {
		return (string) $this->password;
	}

	public function setPassword(string $password): self{
		$this->password = $password;

		return $this;
	}

	/**
	 * @see UserInterface
	 */
	public function getSalt() {
		// not needed when using the "bcrypt" algorithm in security.yaml
	}

	/**
	 * @see UserInterface
	 */
	public function eraseCredentials() {
		// If you store any temporary, sensitive data on the user, clear it here
		// $this->plainPassword = null;
	}

	public function serialize() {
		return serialize(array(
			$this->id,
			$this->username,
			$this->password,
		));
	}

	public function unserialize($serialized) {
		list(
			$this->id,
			$this->username,
			$this->password
		) = unserialize($serialized);
	}

	public function getNombre():  ? string {
		return $this->nombre;
	}

	public function setNombre(string $nombre) : self{
		$this->nombre = $nombre;

		return $this;
	}

	public function getApellido1():  ? string {
		return $this->apellido1;
	}

	public function setApellido1(string $apellido1) : self{
		$this->apellido1 = $apellido1;

		return $this;
	}

	public function getApellido2():  ? string {
		return $this->apellido2;
	}

	public function setApellido2(string $apellido2) : self{
		$this->apellido2 = $apellido2;

		return $this;
	}

	public function getCalle():  ? string {
		return $this->calle;
	}

	public function setCalle(string $calle) : self{
		$this->calle = $calle;

		return $this;
	}

	public function getNumero():  ? int {
		return $this->numero;
	}

	public function setNumero( ? int $numero) : self{
		$this->numero = $numero;

		return $this;
	}

	public function getProvincia() :  ? string {
		return $this->provincia;
	}

	public function setProvincia(string $provincia) : self{
		$this->provincia = $provincia;

		return $this;
	}

	public function getLocalidad():  ? string {
		return $this->localidad;
	}

	public function setLocalidad(string $localidad) : self{
		$this->localidad = $localidad;

		return $this;
	}

	public function getCodigoPostal():  ? int {
		return $this->codigoPostal;
	}

	public function setCodigoPostal(int $codigoPostal) : self{
		$this->codigoPostal = $codigoPostal;

		return $this;
	}

	public function getTelefono():  ? string {
		return $this->telefono;
	}

	public function setTelefono( ? string $telefono) : self{
		$this->telefono = $telefono;

		return $this;
	}

	/**
	 * @return Collection|AlertaInformacion[]
	 */
	public function getAlertas() : Collection {
		return $this->alertas;
	}

	public function addAlerta(AlertaInformacion $alerta): self {
		if (!$this->alertas->contains($alerta)) {
			$this->alertas[] = $alerta;
		}

		return $this;
	}

	public function removeAlerta(AlertaInformacion $alerta): self {
		if ($this->alertas->contains($alerta)) {
			$this->alertas->removeElement($alerta);
		}

		return $this;
	}

	/**
	 * @return Collection|Modificacion[]
	 */
	public function getModificaciones(): Collection {
		return $this->modificaciones;
	}

	public function addModificacione(Modificacion $modificacione): self {
		if (!$this->modificaciones->contains($modificacione)) {
			$this->modificaciones[] = $modificacione;
			$modificacione->setUsuario($this);
		}

		return $this;
	}

	public function removeModificacione(Modificacion $modificacione): self {
		if ($this->modificaciones->contains($modificacione)) {
			$this->modificaciones->removeElement($modificacione);
			// set the owning side to null (unless already changed)
			if ($modificacione->getUsuario() === $this) {
				$modificacione->setUsuario(null);
			}
		}

		return $this;
	}
}
