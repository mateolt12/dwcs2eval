<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlertaInformacionRepository")
 */
class AlertaInformacion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $latitud;

    /**
     * @ORM\Column(type="float")
     */
    private $longitud;

    /**
     * @ORM\Column(type="float")
     */
    private $radioKm;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Usuario", mappedBy="alertas")
     */
    private $usuarios;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Aviso", inversedBy="alertasInformacion")
     */
    private $avisos;

    public function __construct()
    {
        $this->usuarios = new ArrayCollection();
        $this->avisos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLatitud(): ?float
    {
        return $this->latitud;
    }

    public function setLatitud(float $latitud): self
    {
        $this->latitud = $latitud;

        return $this;
    }

    public function getLongitud(): ?float
    {
        return $this->longitud;
    }

    public function setLongitud(float $longitud): self
    {
        $this->longitud = $longitud;

        return $this;
    }

    public function getRadioKm(): ?float
    {
        return $this->radioKm;
    }

    public function setRadioKm(float $radioKm): self
    {
        $this->radioKm = $radioKm;

        return $this;
    }

    /**
     * @return Collection|Usuario[]
     */
    public function getUsuarios(): Collection
    {
        return $this->usuarios;
    }

    public function addUsuario(Usuario $usuario): self
    {
        if (!$this->usuarios->contains($usuario)) {
            $this->usuarios[] = $usuario;
            $usuario->addAlerta($this);
        }

        return $this;
    }

    public function removeUsuario(Usuario $usuario): self
    {
        if ($this->usuarios->contains($usuario)) {
            $this->usuarios->removeElement($usuario);
            $usuario->removeAlerta($this);
        }

        return $this;
    }

    /**
     * @return Collection|Aviso[]
     */
    public function getAvisos(): Collection
    {
        return $this->avisos;
    }

    public function addAviso(Aviso $aviso): self
    {
        if (!$this->avisos->contains($aviso)) {
            $this->avisos[] = $aviso;
        }

        return $this;
    }

    public function removeAviso(Aviso $aviso): self
    {
        if ($this->avisos->contains($aviso)) {
            $this->avisos->removeElement($aviso);
        }

        return $this;
    }
}
