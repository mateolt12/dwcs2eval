<?php
// src/Controller/OlamundoController.php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class OlamundoController extends Controller {
	/**
	 * @route("/olamundo/{nome}", name="olamundo")
	 */
	public function mostrarAction($nome) {
		$mensaxe = 'Ola mundo soy ' . $nome;

		return $this->render("olamundo/mostrar.html.twig", array('mensaxe2' => $mensaxe));

//  return new Response ('<html><body>'.$mensaxe.'</body></html>');

	}
}
?>
