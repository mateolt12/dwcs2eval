<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="users");
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository");
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="name", type="string", length=150)
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string", length=100, unique=true)
	 */
	protected $email;

	/**
	 * @ORM\Column(name="username", type="string", length=100, unique=true)
	 */
	protected $username;

	protected $salt;

	/**
	 * @ORM\Column(name="password", type="string", length=255)
	 * @Serializer\Exclude()
	 */
	protected $password;

	protected $plainPassword;

	/**
	 * @ORM\Column(type="array")
	 */
	protected $roles = [];

	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $createdAt;

	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $updatedAt;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Board", mappedBy="user")
	 */
	private $boards;

	public function __construct() {
		$this->boards = new ArrayCollection();
	}

	public function getId():  ? int {
		return $this->id;
	}

	public function getName() :  ? string {
		return $this->name;
	}

	public function setName(string $name) : self{
		$this->name = $name;

		return $this;
	}

	public function getEmail():  ? string {
		return $this->email;
	}

	public function setEmail(string $email) : self{
		$this->email = $email;

		return $this;
	}

	public function getUsername():  ? string {
		return $this->username;
	}

	public function setUsername(string $username) : self{
		$this->username = $username;

		return $this;
	}

	public function getPassword():  ? string {
		return $this->password;
	}

	public function setPassword(string $password) : self{
		$this->password = $password;

		return $this;
	}

	public function getRoles():  ? array
	{
		return $this->roles;
	}

	public function setRoles(array $roles) : self{
		$this->roles = $roles;

		return $this;
	}

	public function getCreatedAt():  ? \DateTimeInterface {
		return $this->createdAt;
	}

	public function setCreatedAt(\DateTimeInterface $createdAt) : self{
		$this->createdAt = $createdAt;

		return $this;
	}

	public function getUpdatedAt():  ? \DateTimeInterface {
		return $this->updatedAt;
	}

	public function setUpdatedAt(\DateTimeInterface $updatedAt) : self{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPlainPassword() {
		return $this->plainPassword;
	}

	/**
	 * @param $plainPassword
	 */
	public function setPlainPassword($plainPassword) {
		$this->plainPassword = $plainPassword;

		$this->password = null;
	}

	/**
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps() {
		$dateTimeNow = new DateTime('now');
		$this->setUpdatedAt($dateTimeNow);
		if ($this->getCreatedAt() === null) {
			$this->setCreatedAt($dateTimeNow);
		}
	}

	public function getSalt() {}

	public function eraseCredentials() {}

	/**
	 * @return Collection|Board[]
	 */
	public function getBoards(): Collection {
		return $this->boards;
	}

	public function addBoard(Board $board): self {
		if (!$this->boards->contains($board)) {
			$this->boards[] = $board;
			$board->setUser($this);
		}

		return $this;
	}

	public function removeBoard(Board $board): self {
		if ($this->boards->contains($board)) {
			$this->boards->removeElement($board);
			// set the owning side to null (unless already changed)
			if ($board->getUser() === $this) {
				$board->setUser(null);
			}
		}

		return $this;
	}

}
