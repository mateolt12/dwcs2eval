<?php

namespace App\Controller;

use App\Entity\Proba;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProbaController extends AbstractController {
	/**
	 * @Route("/proba", name="proba")
	 */
	public function index() {
		$entityManager = $this->getDoctrine()->getManager();

		$proba = new Proba();
		$proba->setName('Keyboard');
		$proba->setPrice(19.99);
		$proba->setDescription('Ergonomic and stylish!');

		$entityManager->persist($proba);

		$entityManager->flush();

		return new Response('Gardado novo obxecto Proba con id: ' . $proba->getId());
	}
	/**
	 * @Route("/proba/mostrar", name="mostrarTodos")
	 */
	public function mostrarTodosAction() {
		$listaProbas = $this->getDoctrine()->getRepository(Proba::class)->findAll();
		return $this->render('proba/lista.html.twig', ['listaProbas' => $listaProbas]);
	}
	/**
	 * @Route("/proba/mostrar2")
	 */
	public function mostrarTodos2Action() {
		$listaProbas = $this->getDoctrine()->getRepository(Proba::class)->findAll();
		return $this->render('proba/index.html.twig');
	}

	///**
	// * @Route("/proba/{id}", name="proba_mostrar")
	// */
	/*
	public function mostrarAction($id) {
		$proba = $this->getDoctrine()
			->getRepository(Proba::class)
			->find($id);

		if (!$proba) {
			throw $this->createNotFoundException(
				'Non se atopou ningunha proba con id ' . $id
			);
		}
		return new Response('Atoado obxecto con id  ' . $id . ' e nome: ' . $proba->getName());
	}
	*/

	/**
	 * @Route("/proba/{name}", name="proba_mostrar_name")
	 */
	public function mostrarNameAction($name) {

		$proba = $this->getDoctrine()
			->getRepository(Proba::class)
			->findOneBy(['name' => 'Keyboard']);

		if (!$proba) {
			throw $this->createNotFoundException(
				'Non se atopou ningunha proba con nome' . $name
			);
		}
		return new Response('Atopado proba con nome: ' . $proba->getName());
	}
	/**
	 * @Route("/proba/edit/{id}", name="edit")
	 */
	public function updateAction($id) {
		$entityManager = $this->getDoctrine()->getManager();
		$proba = $entityManager->getRepository(Proba::class)->find($id);

		if (!$proba) {
			throw $this->createNotFoundException(
				'Non se atopou ningunha proba con id ' . $id
			);
		}
		$novoNome = 'Novo nome para proba con id ' . $id;
		$proba->setName($novoNome);
		$entityManager->flush();

		return $this->redirectToRoute('proba_mostrar', [
			'id' => $proba->getId(),
		]);
	}

}
