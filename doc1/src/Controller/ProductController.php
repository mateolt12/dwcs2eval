<?php

namespace App\Controller;

use App\Entity\Box;
use App\Entity\Brand;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController {
	/**
	 * @Route("/product", name="product")
	 */
	public function index() {
		$p1 = new Product();
		$p1->setName("Prod 1");
		$p2 = new Product();
		$p2->setName("Prod 2");
		$p3 = new Product();
		$p3->setName("Prod 3");
		$brandA = new Brand();
		$brandA->setName("Marca A");
		$brandB = new Brand();
		$brandB->setName("Marca B");

		$box1 = new Box();
		$box1->setName("Caixa 1");
		$box2 = new Box();
		$box2->setName("Caixa 2");
		$box3 = new Box();
		$box3->setName("Caixa 3");

		$p1->setBrand($brandA);
		$p2->setBrand($brandA);
		$p3->setBrand($brandB);
		$p1->setBox($box1);
		$p2->setBox($box2);
		$p3->setBox($box3);

		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->persist($p1);
		$entityManager->persist($p2);
		$entityManager->persist($p3);
		$entityManager->persist($brandA);
		$entityManager->persist($brandB);
		$entityManager->persist($box1);
		$entityManager->persist($box2);
		$entityManager->persist($box3);

		$entityManager->flush();

		return $this->render('product/index.html.twig', [
			'controller_name' => 'ProductController',
		]);
	}
	/**
	 * @Route("/marca/{id}", name="marca")
	 */
	public function marcaA($id) {
		$entityManager = $this->getDoctrine()->getManager();
		$brand = $entityManager->getRepository(Brand::class)->find($id);
		if (!$brand) {
			throw $this->createNotFoundException(
				"Non no atopuo ningunha proba con id :" . $brand->getId()
			);
		}
		return $this->render('product/brand.html.twig', [
			'products' => $brand->getProducts(),
		]);
	}
}
