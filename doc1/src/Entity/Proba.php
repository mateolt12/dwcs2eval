<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProbaRepository")
 */
class Proba {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $name;

	/**
	 * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
	 */
	private $price;

	/**
	 * @ORM\Column(type="string", length=100, nullable=true)
	 */
	private $description;

	/**
	 * @ORM\Column(type="string", length=50, nullable=true)
	 */
	private $chorrada;

	/**
	 * @ORM\Column(type="string", length=100, nullable=true)
	 */
	private $descripcion;

	public function getId():  ? int {
		return $this->id;
	}

	public function getName() :  ? string {
		return $this->name;
	}

	public function setName(string $name) : self{
		$this->name = $name;

		return $this;
	}

	public function getPrice() {
		return $this->price;
	}

	public function setPrice($price): self{
		$this->price = $price;

		return $this;
	}

	public function getDescription():  ? string {
		return $this->description;
	}

	public function setDescription( ? string $description) : self{
		$this->description = $description;

		return $this;
	}

	public function getChorrada() :  ? string {
		return $this->chorrada;
	}

	public function setChorrada( ? string $chorrada) : self{
		$this->chorrada = $chorrada;

		return $this;
	}

	public function getDescripcion() :  ? string {
		return $this->descripcion;
	}

	public function setDescripcion( ? string $descripcion) : self{
		$this->descripcion = $descripcion;

		return $this;
	}

}
