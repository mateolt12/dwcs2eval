<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Brand", inversedBy="products")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $brand;

	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Box", mappedBy="product", cascade={"persist", "remove"})
	 */
	private $box;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Brand")
	 * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
	 */

	public function getId():  ? int {
		return $this->id;
	}

	public function getName() :  ? string {
		return $this->name;
	}

	public function setName(string $name) : self{
		$this->name = $name;

		return $this;
	}

	public function getBrand():  ? Brand {
		return $this->brand;
	}

	public function setBrand( ? Brand $brand) : self{
		$this->brand = $brand;

		return $this;
	}

	public function getBox() :  ? Box {
		return $this->box;
	}

	public function setBox(Box $box) : self{
		$this->box = $box;

		// set the owning side of the relation if necessary
		if ($this !== $box->getProduct()) {
			$box->setProduct($this);
		}

		return $this;
	}
}
