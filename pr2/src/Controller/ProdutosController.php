<?php
// src/Controller/ProdutosController.php
namespace App\Controller;
use App\Entity\Produto;
use App\Form\ProdutoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProdutosController extends Controller {
	/**
	 * @Route("/produtos/new", name="newProduto")
	 */

	public function new (Request $request) {
		$produto = new Produto(['idProduto' => 1, 'nome' => 'prod1']);
		$form = $this->createForm(ProdutoType::class, $produto);

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			//$form->getData();

			//return $this->redirect();

			return $this->render("produtos/exito.html.twig", array('produto' => $produto));

		}

		return $this->render('produtos/new.html.twig', array('form' => $form->createView()));

	}
}

?>