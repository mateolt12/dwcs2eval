<?php
// src/suma/SumaController.php
namespace App\Controller\suma;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class SumaController extends Controller {
	/**
	 * @Route("/suma/{n1}/{n2}", name="suma")
	 */

	public function mostrarAction($n1, $n2) {
		$resultado = "Resultado: " . $n1 . " + " . $n2 . " = ";
		$operacion = $n1 + $n2;
		return $this->render("suma/mostrar.html.twig", array('resultado' => $resultado, 'operacion' => $operacion));

	}
}
?>

