<?php
//src/Entity/Produto.php
namespace App\Entity;
/**
 *
 */
use Symfony\Component\Validator\Constraints as Assert;

class Produto implements \JsonSerializable {
/**
 *@Assert\NotBlank(message = "Este campo no puede estar vacio")
 *
 *
 */
	private $idProduto;
	/**
	 *@Assert\Length(min = 5, minMessage = "Lonxitude mínima de {{limit}} caracteres")
	 *
	 *
	 */
	private $nome;

	function __construct(array $datos) {
		foreach ($datos as $campo => $valor) {
			$this->$campo = $valor;
		}
	}

	/**
	 * Get idProduto
	 * @return
	 */
	public function getIdProduto() {
		return $this->idProduto;
	}

	/**
	 * Set idProduto
	 * @return $this
	 */
	public function setIdProduto($idProduto) {
		$this->idProduto = $idProduto;
		return $this;
	}

	/**
	 * Get nome
	 * @return
	 */
	public function getNome() {
		return $this->nome;
	}

	/**
	 * Set nome
	 * @return $this
	 */
	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}

	public function jsonSerialize() {
		$vars = get->object->vars($this);
		return $vars;
	}

}

?>