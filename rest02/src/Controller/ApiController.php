<?php

namespace App\Controller;
use App\Entity\Empleado;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 *
 * @Route("/api")
 */

class ApiController extends AbstractController {
	/**
	 * @Route("/algo", name="api")
	 */
	public function index() {

		return $this->render('api/index.html.twig', [
			'controller_name' => 'ApiController',
		]);
	}
	/**
	 * @Rest\Get("/empleado", name="listarEmpleados")
	 */
	public function getAllEmpleados(SerializerInterface $serializer) {
		$em = $this->getDoctrine()->getManager();
		$empleados = [];
		$message = "";

		try {
			$empleados = $em->getRepository(Empleado::Class)->findAll();

			if (is_null($empleados)) {
				$empleados = [];
			}

			$code = 200;
			$error = false;

		} catch (Exception $ex) {
			$code = 500;
			$error = true;
			$message = "An error has occurred trying to get all Empleados - Error: {$ex->getMessage()}";

		}

		$response = [
			'code' => $code,
			'error' => $error,
			'data' => $code == 200 ? $empleados : $message,
		];

		return new Response($serializer->serialize($response, "json"));

	}

	/**
	 * @Rest\Get("/empleado/{id}", name="listarEmpleado")
	 */
	public function getEmpleado(SerializerInterface $serializer, $id) {
		$em = $this->getDoctrine()->getManager();
		$empleado = [];
		$message = "";

		try {
			$empleado = $em->getRepository(Empleado::Class)->find($id);

			if (is_null($empleado)) {
				$empleado = [];
			}

			$code = 200;
			$error = false;

		} catch (Exception $ex) {
			$code = 500;
			$error = true;
			$message = "An error has occurred trying to get all Empleados - Error: {$ex->getMessage()}";

		}

		$response = [
			'code' => $code,
			'error' => $error,
			'data' => $code == 200 ? $empleado : $message,
		];

		return new Response($serializer->serialize($response, "json"));

	}

	/**
	 * @Rest\Post("/empleado/new", name="crear empleado")
	 */
	public function nuevoEmpleado(SerializerInterface $serializer, Request $request) {
		$em = $this->getDoctrine()->getManager();
		$message = "";

		try {
			$nome = $request->request->get("nombre", null);
			$sueldo = $request->request->get("sueldo", null);

			if (!is_null($nome) && !is_null($sueldo)) {
				$empleado = new Empleado();
				$empleado->setNombre($nome);
				$empleado->setSueldo($sueldo);
				$em->persist($empleado);
				$em->flush();
				$code = 200;
				$error = false;
			}

		} catch (Exception $ex) {
			$code = 500;
			$error = true;
			$message = "An error has occurred trying to create new empleado - Error: {$ex->getMessage()}";

		}

		$response = [
			'code' => $code,
			'error' => $error,
			'data' => $code == 200 ? "Ok" : $message,
		];

		return new Response($serializer->serialize($response, "json"));
	}

	/**
	 * @Rest\Delete("/empleado/del/{id}", name="borrar_empleado")
	 */
	public function borrarEmpleado(SerializerInterface $serializer, $id) {

		$em = $this->getDoctrine()->getManager();
		$message = "";

		try {
			$code = 200;
			$error = false;
			$empleado = $em->getRepository(Empleado::Class)->find($id);

			$em->remove($empleado);
			$em->flush();
			$message = "Empleado eliminado correctamente.";

		} catch (Exception $ex) {
			$code = 500;
			$error = true;
			$message = "An error has occurred trying to remove the currrent board - Error: {$ex->getMessage()}";
		}

		$response = [
			'code' => $code,
			'error' => $error,
			'data' => $code == 200 ? "Ok" : $message,
		];
		return new Response($serializer->serialize($response, "json"));
	}

	/**
	 * @Rest\Put("/empleado/edit/{id}", name="editar_empleado")
	 */
	public function editarEmpleado(SerializerInterface $serializer, $id, Request $request) {

		$em = $this->getDoctrine()->getManager();
		$message = "";

		try {
			$code = 200;
			$error = false;
			$nome = $request->request->get("nombre", null);
			$sueldo = $request->request->get("sueldo", null);
			$empleado = $em->getRepository(Empleado::Class)->find($id);

			$empleado->setNombre($nome);
			$empleado->setSueldo($sueldo);

			$em->persist($empleado);
			$em->flush();
			$message = "Empleado editado correctamente.";

		} catch (Exception $ex) {
			$code = 500;
			$error = true;
			$message = "An error has occurred trying to remove the currrent board - Error: {$ex->getMessage()}";
		}

		$response = [
			'code' => $code,
			'error' => $error,
			'data' => $code == 200 ? "Ok" : $message,
		];
		return new Response($serializer->serialize($response, "json"));
	}
}
