<?php

namespace App\Controller;

use App\Entity\Empleado;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoadDataController extends AbstractController {
	/**
	 * @Route("/loadData", name="load_data")
	 */
	public function loadData() {
		$em = $this->getDoctrine()->getManager();

		$empleado1 = new Empleado();
		$empleado1->setNombre("Luis Miguel");
		$empleado1->setSueldo(5000.0);

		$empleado2 = new Empleado();
		$empleado2->setNombre("Aida");
		$empleado2->setSueldo(12000.0);

		$em->persist($empleado1);
		$em->persist($empleado2);
		$em->flush();

		return new Response("Datos introducidos correctamente, ma nigga.");

	}
}
