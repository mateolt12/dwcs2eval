<?php

namespace App\Controller;
use App\Entity\Produto;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 *
 * @Route("/api")
 */

class ApiController extends AbstractController {
	/**
	 * @Route("/index", name="api")
	 */
	public function index() {
		return $this->render('api/index.html.twig', [
			'controller_name' => 'ApiController',
		]);
	}

	/**
	 * @Rest\Get("/produtos", name="listar_produtos")
	 */
	public function getAllProdutos(SerializerInterface $serializer) {

		//$serializer = $this->get('jms_serializer');
		$em = $this->getDoctrine()->getManager();
		$produtos = [];
		$message = "";

		try {
			$produtos = $em->getRepository(Produto::Class)->findAll();

			if (is_null($produtos)) {
				$produtos = [];
			}

			$code = 200;
			$error = false;

		} catch (Exception $ex) {
			$code = 500;
			$error = true;
			$message = "An error has occurred trying to get all Empleados - Error: {$ex->getMessage()}";

		}

		$response = [
			'code' => $code,
			'error' => $error,
			'data' => $code == 200 ? $produtos : $message,
		];

		return new Response($serializer->serialize($response, "json"));
	}

	/**
	 * @Rest\Get("/produto/{id}", name="listar_produto")
	 */
	public function getProduto(SerializerInterface $serializer, $id) {
		$em = $this->getDoctrine()->getManager();
		$produto = [];
		$message = "";

		try {
			$produto = $em->getRepository(Produto::Class)->find($id);

			if (is_null($produto)) {
				$produto = [];
			}

			$code = 200;
			$error = false;

		} catch (Exception $ex) {
			$code = 500;
			$error = true;
			$message = "An error has occurred trying to get all Empleados - Error: {$ex->getMessage()}";

		}

		$response = [
			'code' => $code,
			'error' => $error,
			'data' => $code == 200 ? $produto : $message,
		];

		return new Response($serializer->serialize($response, "json"));

	}

	/**
	 * @Rest\Post("/produto/new", name="crear_produto")
	 */
	public function nuevoProduto(SerializerInterface $serializer, Request $request) {
		$em = $this->getDoctrine()->getManager();
		$message = "";

		try {
			$nome = $request->request->get("nombre", null);
			$prezo = $request->request->get("prezo", null);
			$code = 200;
			$error = false;

			if (!is_null($nome) && !is_null($prezo)) {
				$produto = new Produto();
				$produto->setNombre($nome);
				$produto->setPrezo($prezo);
				$em->persist($produto);
				$em->flush();

			}

		} catch (Exception $ex) {
			$code = 500;
			$error = true;
			$message = "An error has occurred trying to create new empleado - Error: {$ex->getMessage()}";

		}

		$response = [
			'code' => $code,
			'error' => $error,
			'data' => $code == 200 ? "Ok" : $message,
		];

		return new Response($serializer->serialize($response, "json"));
	}

	/**
	 * @Rest\Delete("/produto/del/{id}", name="borrar_produto")
	 */
	public function borrarProduto(SerializerInterface $serializer, $id) {

		$em = $this->getDoctrine()->getManager();
		$message = "";

		try {
			$code = 200;
			$error = false;
			$produto = $em->getRepository(Produto::Class)->find($id);

			$em->remove($produto);
			$em->flush();

		} catch (Exception $ex) {
			$code = 500;
			$error = true;
			$message = "An error has occurred trying to remove the currrent board - Error: {$ex->getMessage()}";
		}

		$response = [
			'code' => $code,
			'error' => $error,
			'data' => $code == 200 ? "Produtuo eliminado correctamente." : $message,
		];
		return new Response($serializer->serialize($response, "json"));
	}

	/**
	 * @Rest\Put("/produto/edit/{id}", name="editar_produto")
	 */
	public function editarEmpleado(SerializerInterface $serializer, $id, Request $request) {

		$em = $this->getDoctrine()->getManager();
		$message = "";

		try {
			$code = 200;
			$error = false;
			$nome = $request->request->get("nombre", null);
			$prezo = $request->request->get("prezo", null);
			$produto = $em->getRepository(Produto::Class)->find($id);

			$produto->setNombre($nome);
			$produto->setSueldo($prezo);

			$em->persist($produto);
			$em->flush();

		} catch (Exception $ex) {
			$code = 500;
			$error = true;
			$message = "An error has occurred trying to remove the currrent board - Error: {$ex->getMessage()}";
		}

		$response = [
			'code' => $code,
			'error' => $error,
			'data' => $code == 200 ? "Produto editado correctamente." : $message,
		];
		return new Response($serializer->serialize($response, "json"));
	}

}
