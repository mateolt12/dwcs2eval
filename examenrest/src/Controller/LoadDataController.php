<?php

namespace App\Controller;
use App\Entity\Cliente;
use App\Entity\Pedido;
use App\Entity\Produto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoadDataController extends AbstractController {
	/**
	 * @Route("/load/data", name="load_data")
	 */
	public function index() {
		return $this->render('load_data/index.html.twig', [
			'controller_name' => 'LoadDataController',
		]);
	}

	/**
	 * @Route ("/loaddata", name="loaddata")
	 */
	public function loaddata() {
		$p1 = new Produto();
		$p1->setNome("Prod 1");
		$p1->setPrezo(12.45);
		$p2 = new Produto();
		$p2->setNome("Prod 2");
		$p2->setPrezo(34.56);
		$p3 = new Produto();
		$p3->setNome("Prod 3");
		$p3->setPrezo(134.56);

		$c1 = new Cliente();
		$c1->setNome("Xan");
		$c2 = new Cliente();
		$c2->setNome("Pedro");
		$c3 = new Cliente();
		$c3->setNome("Manuel");

		$pe1 = new Pedido();
		$pe1->setProduto($p1);
		$pe1->setData(new \DateTime('2012-02-03 15:34:56'));
		$pe1->setCliente($c1);
		$pe1->setUnidades(11);

		$pe2 = new Pedido();
		$pe2->setProduto($p1);
		$pe2->setData(new \DateTime('2012-02-22 15:34:56'));
		$pe2->setCliente($c1);
		$pe2->setUnidades(111);

		$pe3 = new Pedido();
		$pe3->setProduto($p2);
		$pe3->setData(new \DateTime('2012-02-15 15:34:56'));
		$pe3->setCliente($c2);
		$pe3->setUnidades(22);

		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->persist($p1);
		$entityManager->persist($p2);
		$entityManager->persist($p3);
		$entityManager->persist($c1);
		$entityManager->persist($c2);
		$entityManager->persist($c3);
		$entityManager->persist($pe1);
		$entityManager->persist($pe2);
		$entityManager->persist($pe3);

		$entityManager->flush();

		return new Response("datos cargados OK");
	}

}
