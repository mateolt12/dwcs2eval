<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PedidoRepository")
 */
class Pedido {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $unidades;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Produto", inversedBy="pedidos")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $produto;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $data;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Cliente", inversedBy="pedidos")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $cliente;

	public function getId():  ? int {
		return $this->id;
	}

	public function getUnidades() :  ? int {
		return $this->unidades;
	}

	public function setUnidades(int $unidades) : self{
		$this->unidades = $unidades;

		return $this;
	}

	public function getProduto():  ? Produto {
		return $this->produto;
	}

	public function setProduto( ? Produto $produto) : self{
		$this->produto = $produto;

		return $this;
	}

	public function getData() :  ? \DateTimeInterface {
		return $this->data;
	}

	public function setData(\DateTimeInterface $data) : self{
		$this->data = $data;

		return $this;
	}

	public function getCliente():  ? Cliente {
		return $this->cliente;
	}

	public function setCliente( ? Cliente $cliente) : self{
		$this->cliente = $cliente;

		return $this;
	}

}
