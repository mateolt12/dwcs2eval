<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProdutoRepository")
 */
class Produto {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $nome;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Pedido", mappedBy="produto", orphanRemoval=true)
	 */
	private $pedidos;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	private $prezo;

	public function __construct() {
		$this->pedidos = new ArrayCollection();
	}

	public function getId():  ? int {
		return $this->id;
	}

	public function getNome() :  ? string {
		return $this->nome;
	}

	public function setNome(string $nome) : self{
		$this->nome = $nome;

		return $this;
	}

	/**
	 * @return Collection|Pedido[]
	 */
	public function getPedidos(): Collection {
		return $this->pedidos;
	}

	public function addPedido(Pedido $pedido): self {
		if (!$this->pedidos->contains($pedido)) {
			$this->pedidos[] = $pedido;
			$pedido->setProduto($this);
		}

		return $this;
	}

	public function removePedido(Pedido $pedido): self {
		if ($this->pedidos->contains($pedido)) {
			$this->pedidos->removeElement($pedido);
			// set the owning side to null (unless already changed)
			if ($pedido->getProduto() === $this) {
				$pedido->setProduto(null);
			}
		}

		return $this;
	}

	public function getPrezo():  ? float {
		return $this->prezo;
	}

	public function setPrezo( ? float $prezo) : self{
		$this->prezo = $prezo;

		return $this;
	}
}
