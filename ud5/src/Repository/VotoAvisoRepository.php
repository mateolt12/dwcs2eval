<?php

namespace App\Repository;

use App\Entity\VotoAviso;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method VotoAviso|null find($id, $lockMode = null, $lockVersion = null)
 * @method VotoAviso|null findOneBy(array $criteria, array $orderBy = null)
 * @method VotoAviso[]    findAll()
 * @method VotoAviso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VotoAvisoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, VotoAviso::class);
    }

    // /**
    //  * @return VotoAviso[] Returns an array of VotoAviso objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?VotoAviso
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
