<?php

namespace App\Repository;

use App\Entity\Basico;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Basico|null find($id, $lockMode = null, $lockVersion = null)
 * @method Basico|null findOneBy(array $criteria, array $orderBy = null)
 * @method Basico[]    findAll()
 * @method Basico[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasicoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Basico::class);
    }

    // /**
    //  * @return Basico[] Returns an array of Basico objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Basico
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
