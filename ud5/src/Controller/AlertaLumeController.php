<?php

namespace App\Controller;

use App\Entity\Administrador;
use App\Entity\AlertaInformacion;
use App\Entity\Aviso;
use App\Entity\Basico;
use App\Entity\VotoAviso;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AlertaLumeController extends AbstractController {
	/**
	 * @Route("/alerta/lume", name="alerta_lume")
	 */
	public function index() {
		return $this->render('alerta_lume/index.html.twig', [
			'controller_name' => 'AlertaLumeController',
		]);
	}

	/**
	 * @Route("/loaddata", name="load data")
	 */
	public function loadAction() {
		$entityManager = $this->getDoctrine()->getManager();

		$basico = new Basico();
		$basico->setNombre("Luis Miguel");
		$basico->setApellido1("Garcia");
		$basico->setApellido2("Rodriguez");
		$basico->setDireccion("Calle de los repetidores");
		$basico->setLocalidad("Mañente");
		$basico->setProvincia("Lugo");
		$basico->setCodigoPostal("27419");
		$basico->setTelefono("666666666");

		$administrador = new Administrador();
		$administrador->setNombre("Mateo");
		$administrador->setApellido1("López");
		$administrador->setApellido2("Timiraos");
		$administrador->setDireccion("Calle Doctor Casares 98");
		$administrador->setLocalidad("Monforte de Lemos");
		$administrador->setProvincia("Lugo");
		$administrador->setCodigoPostal("27400");
		$administrador->setTelefono("638251901");

		$alerta1 = new AlertaInformacion();
		$alerta1->setLatitud(48.37982);
		$alerta1->setLongitud(-36.35578);
		$alerta1->setRadioKm(10.0);

		$alerta2 = new AlertaInformacion();
		$alerta2->setLatitud(47.37982);
		$alerta2->setLongitud(-29.35578);
		$alerta2->setRadioKm(5.0);

		$aviso1 = new Aviso();
		$aviso1->setComentarioCreador("Incendio en Ferrol.");

		$aviso1->setFecha(new \DateTime());
		$aviso1->setTipo("Incendio");
		$aviso1->setIPCreador("192.168.0.13");
		$aviso1->setLatitud(43.4907942);
		$aviso1->setLongitud(-8.2397163);

		$aviso2 = new Aviso();
		$aviso2->setComentarioCreador("Incendio en Santiago.");

		$aviso2->setFecha(new \DateTime());
		$aviso2->setTipo("Incendio");
		$aviso2->setIpCreador("192.168.0.14");
		$aviso2->setLatitud(42.8802625);
		$aviso2->setLongitud(-8.5797892);

		$voto1 = new VotoAviso();
		$voto1->setFecha(new \DateTime());
		$voto1->setComentario("Incendio verídico.");
		$voto1->setIP("192.168.0.15");
		$voto1->setTipoVoto("positivo");
		$voto1->setNumVoto(5);

		$voto2 = new VotoAviso();
		$voto2->setFecha(new \DateTime());
		$voto2->setComentario("Incendio non verídico.");
		$voto2->setIP("192.168.0.16");
		$voto2->setTipoVoto("negativo");
		$voto2->setNumVoto(4);

		$basico->addAlerta($alerta1);
		$alerta1->addAviso($aviso1);
		$aviso1->addVoto($voto1);
		$voto1->setAviso($aviso1);

		$basico->addAlerta($alerta2);
		$alerta2->addAviso($aviso2);
		$aviso2->addVoto($voto2);
		$voto2->setAviso($aviso2);

		$entityManager->persist($basico);
		$entityManager->persist($administrador);
		$entityManager->persist($alerta1);
		$entityManager->persist($aviso1);
		$entityManager->persist($voto1);
		$entityManager->persist($alerta2);
		$entityManager->persist($aviso2);
		$entityManager->persist($voto2);

		$entityManager->flush();

		return new Response("Se han almacenado los datos correctamente.");
	}

	/**
	 * @Route("/basico/{id}/alertas", name="alertas_basico")
	 */
	public function alertasBasico($id) {

		$entityManager = $this->getDoctrine()->getManager();
		$usuario = $entityManager->getRepository(Basico::class)->find($id);
		if (!$usuario) {
			throw $this->createNotFoundException(
				"Non existe ese usuario básico co id: " . $id
			);
		}

		return $this->render('alerta_lume/alertas.html.twig', ['id' => $id, 'alertas' => $usuario->getAlertas()]);
	}

	/**
	 * @Route("/aviso/{id}/votosaviso", name="votos_dos_avisos")
	 */
	public function mostrarVotosAvisos($id) {
		$entityManager = $this->getDoctrine()->getManager();
		$votosAvisos = $entityManager->getRepository(VotoAviso::class)->find($id);

		if (!$votosAvisos) {
			throw $this->createNotFoundException(
				"Non se atoparon votos co id " . $id);
		}

		return new Response("Id: " . $votosAvisos->getId() . " - Número de avisos: " . $votosAvisos->getNumVoto() . "<br>");

	}

	/**
	 * @Route("/avisos", name="avisos")
	 */
	public function mostrarAvisos() {
		$entityManager = $this->getDoctrine()->getManager();
		$avisos = $entityManager->getRepository(Aviso::class)->findAll();
		if (!$avisos) {
			throw $this->createNotFoundException(
				"Non se atoparon avisos."
			);
		}
		return $this->render('alerta_lume/avisos.html.twig', array("avisos" => $avisos));
	}

	/**
	 * @Route("/basico/{id1}/alerta/{id2} ", name="alerta_por_id_basico")
	 */

	public function alertaPorIdBasico($id1, $id2) {

		$entityManager = $this->getDoctrine()->getManager();
		$usuario = $entityManager->getRepository(Basico::class)->find($id1);
		if (!$usuario) {
			throw $this->createNotFoundException(
				"Non existe ese usuario básico co id: " . $id1
			);
		}

		$alertas = $usuario->getAlertas();
		$alertaId2 = null;
		foreach ($alertas as $alerta) {
			if ($alerta->getId() == $id2) {
				$alertaId2 = $alerta;
				break;
			}
		}

		if ($alertaId2 == null) {
			throw $this->createNotFoundException("Non existe esa alerta co id: " . $id2);
		}

		return $this->render('alerta_lume/alerta.html.twig', ['id' => $id1, 'alertas' => $alertaId2]);
	}

}
