<?php

namespace App\Entity;

use App\Entity\Usuario;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BasicoRepository")
 */
class Basico extends Usuario {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AlertaInformacion", inversedBy="basicos")
     */
    private $alertas;

    public function __construct()
    {
        $this->alertas = new ArrayCollection();
    }

	public function getId():  ? int {
                        		return $this->id;
                        	}

    /**
     * @return Collection|AlertaInformacion[]
     */
    public function getAlertas(): Collection
    {
        return $this->alertas;
    }

    public function addAlerta(AlertaInformacion $alerta): self
    {
        if (!$this->alertas->contains($alerta)) {
            $this->alertas[] = $alerta;
        }

        return $this;
    }

    public function removeAlerta(AlertaInformacion $alerta): self
    {
        if ($this->alertas->contains($alerta)) {
            $this->alertas->removeElement($alerta);
        }

        return $this;
    }
}
