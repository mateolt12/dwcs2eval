<?php

namespace App\Entity;

use App\Entity\Usuario;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdministradorRepository")
 */
class Administrador extends Usuario {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\VotoAviso", inversedBy="administradors")
     */
    private $votos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Aviso", inversedBy="administradors")
     */
    private $aviso;

    public function __construct()
    {
        $this->votos = new ArrayCollection();
        $this->aviso = new ArrayCollection();
    }

	public function getId():  ? int {
                                       		return $this->id;
                                       	}

    /**
     * @return Collection|VotoAviso[]
     */
    public function getVotos(): Collection
    {
        return $this->votos;
    }

    public function addVoto(VotoAviso $voto): self
    {
        if (!$this->votos->contains($voto)) {
            $this->votos[] = $voto;
        }

        return $this;
    }

    public function removeVoto(VotoAviso $voto): self
    {
        if ($this->votos->contains($voto)) {
            $this->votos->removeElement($voto);
        }

        return $this;
    }

    /**
     * @return Collection|Aviso[]
     */
    public function getAviso(): Collection
    {
        return $this->aviso;
    }

    public function addAviso(Aviso $aviso): self
    {
        if (!$this->aviso->contains($aviso)) {
            $this->aviso[] = $aviso;
        }

        return $this;
    }

    public function removeAviso(Aviso $aviso): self
    {
        if ($this->aviso->contains($aviso)) {
            $this->aviso->removeElement($aviso);
        }

        return $this;
    }
}
