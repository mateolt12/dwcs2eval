<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlertaInformacionRepository")
 */
class AlertaInformacion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $radioKm;

    /**
     * @ORM\Column(type="float")
     */
    private $latitud;

    /**
     * @ORM\Column(type="float")
     */
    private $longitud;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Basico", mappedBy="alertas")
     */
    private $basicos;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Aviso", inversedBy="alertaInformacions")
     */
    private $avisos;

    public function __construct()
    {
        $this->basicos = new ArrayCollection();
        $this->avisos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRadioKm(): ?float
    {
        return $this->radioKm;
    }

    public function setRadioKm(float $radioKm): self
    {
        $this->radioKm = $radioKm;

        return $this;
    }

    public function getLatitud(): ?float
    {
        return $this->latitud;
    }

    public function setLatitud(float $latitud): self
    {
        $this->latitud = $latitud;

        return $this;
    }

    public function getLongitud(): ?float
    {
        return $this->longitud;
    }

    public function setLongitud(float $longitud): self
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * @return Collection|Basico[]
     */
    public function getBasicos(): Collection
    {
        return $this->basicos;
    }

    public function addBasico(Basico $basico): self
    {
        if (!$this->basicos->contains($basico)) {
            $this->basicos[] = $basico;
            $basico->addAlerta($this);
        }

        return $this;
    }

    public function removeBasico(Basico $basico): self
    {
        if ($this->basicos->contains($basico)) {
            $this->basicos->removeElement($basico);
            $basico->removeAlerta($this);
        }

        return $this;
    }

    /**
     * @return Collection|Aviso[]
     */
    public function getAvisos(): Collection
    {
        return $this->avisos;
    }

    public function addAviso(Aviso $aviso): self
    {
        if (!$this->avisos->contains($aviso)) {
            $this->avisos[] = $aviso;
        }

        return $this;
    }

    public function removeAviso(Aviso $aviso): self
    {
        if ($this->avisos->contains($aviso)) {
            $this->avisos->removeElement($aviso);
        }

        return $this;
    }
}
