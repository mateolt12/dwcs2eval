<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VotoAvisoRepository")
 */
class VotoAviso
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ip;

    /**
     * @ORM\Column(type="boolean")
     */
    private $tipoVoto;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $comentario;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Administrador", mappedBy="votos")
     */
    private $administradors;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Aviso", inversedBy="votos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $aviso;

    /**
     * @ORM\Column(type="integer")
     */
    private $numVoto;

    public function __construct()
    {
        $this->administradors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getTipoVoto(): ?bool
    {
        return $this->tipoVoto;
    }

    public function setTipoVoto(bool $tipoVoto): self
    {
        $this->tipoVoto = $tipoVoto;

        return $this;
    }

    public function getComentario(): ?string
    {
        return $this->comentario;
    }

    public function setComentario(string $comentario): self
    {
        $this->comentario = $comentario;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * @return Collection|Administrador[]
     */
    public function getAdministradors(): Collection
    {
        return $this->administradors;
    }

    public function addAdministrador(Administrador $administrador): self
    {
        if (!$this->administradors->contains($administrador)) {
            $this->administradors[] = $administrador;
            $administrador->addVoto($this);
        }

        return $this;
    }

    public function removeAdministrador(Administrador $administrador): self
    {
        if ($this->administradors->contains($administrador)) {
            $this->administradors->removeElement($administrador);
            $administrador->removeVoto($this);
        }

        return $this;
    }

    public function getAviso(): ?Aviso
    {
        return $this->aviso;
    }

    public function setAviso(?Aviso $aviso): self
    {
        $this->aviso = $aviso;

        return $this;
    }

    public function getNumVoto(): ?int
    {
        return $this->numVoto;
    }

    public function setNumVoto(int $numVoto): self
    {
        $this->numVoto = $numVoto;

        return $this;
    }
}
