<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="rol", type="string")
 * @ORM\DiscriminatorMap( {"administrador" = "Administrador", "basico" = "Basico"} )
 */
abstract class Usuario {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $nombre;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $apellido1;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $apellido2;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $telefono;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $direccion;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $localidad;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $provincia;

	/**
	 * @ORM\Column(type="integer")
	 */
	private $codigoPostal;

	public function getId():  ? int {
		return $this->id;
	}

	public function getNombre() :  ? string {
		return $this->nombre;
	}

	public function setNombre(string $nombre) : self{
		$this->nombre = $nombre;

		return $this;
	}

	public function getApellido1():  ? string {
		return $this->apellido1;
	}

	public function setApellido1(string $apellido1) : self{
		$this->apellido1 = $apellido1;

		return $this;
	}

	public function getApellido2():  ? string {
		return $this->apellido2;
	}

	public function setApellido2(string $apellido2) : self{
		$this->apellido2 = $apellido2;

		return $this;
	}

	public function getTelefono():  ? int {
		return $this->telefono;
	}

	public function setTelefono(int $telefono) : self{
		$this->telefono = $telefono;

		return $this;
	}

	public function getDireccion():  ? string {
		return $this->direccion;
	}

	public function setDireccion(string $direccion) : self{
		$this->direccion = $direccion;

		return $this;
	}

	public function getLocalidad():  ? string {
		return $this->localidad;
	}

	public function setLocalidad(string $localidad) : self{
		$this->localidad = $localidad;

		return $this;
	}

	public function getProvincia():  ? string {
		return $this->provincia;
	}

	public function setProvincia(string $provincia) : self{
		$this->provincia = $provincia;

		return $this;
	}

	public function getCodigoPostal():  ? int {
		return $this->codigoPostal;
	}

	public function setCodigoPostal(int $codigoPostal) : self{
		$this->codigoPostal = $codigoPostal;

		return $this;
	}
}
