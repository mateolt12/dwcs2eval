<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AvisoRepository")
 */
class Aviso {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="float")
	 */
	private $latitud;

	/**
	 * @ORM\Column(type="float")
	 */
	private $longitud;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $tipo;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $ipCreador;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $fecha;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $comentarioCreador;

	/**
	 * @ORM\Column(type="blob", nullable=true)
	 */
	private $imagen;

	/**
	 * @ORM\ManyToMany(targetEntity="App\Entity\Administrador", mappedBy="aviso")
	 */
	private $administradors;

	/**
	 * @ORM\ManyToMany(targetEntity="App\Entity\AlertaInformacion", mappedBy="avisos")
	 */
	private $alertaInformacions;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\VotoAviso", mappedBy="aviso", orphanRemoval=true)
	 */
	private $votos;

	public function __construct() {
		$this->administradors = new ArrayCollection();
		$this->alertaInformacions = new ArrayCollection();
		$this->votos = new ArrayCollection();
	}

	public function getId():  ? int {
		return $this->id;
	}

	public function getLatitud() :  ? float {
		return $this->latitud;
	}

	public function setLatitud(float $latitud) : self{
		$this->latitud = $latitud;

		return $this;
	}

	public function getLongitud():  ? float {
		return $this->longitud;
	}

	public function setLongitud(float $longitud) : self{
		$this->longitud = $longitud;

		return $this;
	}

	public function getTipo():  ? string {
		return $this->tipo;
	}

	public function setTipo(string $tipo) : self{
		$this->tipo = $tipo;

		return $this;
	}

	public function getIpCreador():  ? string {
		return $this->ipCreador;
	}

	public function setIpCreador(string $ipCreador) : self{
		$this->ipCreador = $ipCreador;

		return $this;
	}

	public function getFecha():  ? \DateTimeInterface {
		return $this->fecha;
	}

	public function setFecha(\DateTimeInterface $fecha) : self{
		$this->fecha = $fecha;

		return $this;
	}

	public function getComentarioCreador():  ? string {
		return $this->comentarioCreador;
	}

	public function setComentarioCreador( ? string $comentarioCreador) : self{
		$this->comentarioCreador = $comentarioCreador;

		return $this;
	}

	public function getImagen() {
		return $this->imagen;
	}

	public function setImagen($imagen) : self{
		$this->imagen = $imagen;

		return $this;
	}

	/**
	 * @return Collection|Administrador[]
	 */
	public function getAdministradors(): Collection {
		return $this->administradors;
	}

	public function addAdministrador(Administrador $administrador): self {
		if (!$this->administradors->contains($administrador)) {
			$this->administradors[] = $administrador;
			$administrador->addAviso($this);
		}

		return $this;
	}

	public function removeAdministrador(Administrador $administrador): self {
		if ($this->administradors->contains($administrador)) {
			$this->administradors->removeElement($administrador);
			$administrador->removeAviso($this);
		}

		return $this;
	}

	/**
	 * @return Collection|AlertaInformacion[]
	 */
	public function getAlertaInformacions(): Collection {
		return $this->alertaInformacions;
	}

	public function addAlertaInformacion(AlertaInformacion $alertaInformacion): self {
		if (!$this->alertaInformacions->contains($alertaInformacion)) {
			$this->alertaInformacions[] = $alertaInformacion;
			$alertaInformacion->addAviso($this);
		}

		return $this;
	}

	public function removeAlertaInformacion(AlertaInformacion $alertaInformacion): self {
		if ($this->alertaInformacions->contains($alertaInformacion)) {
			$this->alertaInformacions->removeElement($alertaInformacion);
			$alertaInformacion->removeAviso($this);
		}

		return $this;
	}

	/**
	 * @return Collection|VotoAviso[]
	 */
	public function getVotos(): Collection {
		return $this->votos;
	}

	public function addVoto(VotoAviso $voto): self {
		if (!$this->votos->contains($voto)) {
			$this->votos[] = $voto;
			$voto->setAviso($this);
		}

		return $this;
	}

	public function removeVoto(VotoAviso $voto): self {
		if ($this->votos->contains($voto)) {
			$this->votos->removeElement($voto);
			// set the owning side to null (unless already changed)
			if ($voto->getAviso() === $this) {
				$voto->setAviso(null);
			}
		}

		return $this;
	}
}
