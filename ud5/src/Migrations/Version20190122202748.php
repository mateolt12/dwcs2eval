<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190122202748 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE voto_aviso (id INT AUTO_INCREMENT NOT NULL, aviso_id INT NOT NULL, ip VARCHAR(255) NOT NULL, tipo_voto TINYINT(1) NOT NULL, comentario VARCHAR(255) NOT NULL, fecha DATETIME NOT NULL, num_voto INT NOT NULL, INDEX IDX_4724CD769D17A5A7 (aviso_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, apellido1 VARCHAR(255) NOT NULL, apellido2 VARCHAR(255) NOT NULL, telefono INT NOT NULL, direccion VARCHAR(255) NOT NULL, localidad VARCHAR(255) NOT NULL, provincia VARCHAR(255) NOT NULL, codigo_postal INT NOT NULL, rol VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE administrador_voto_aviso (administrador_id INT NOT NULL, voto_aviso_id INT NOT NULL, INDEX IDX_BB8E155A48DFEBB7 (administrador_id), INDEX IDX_BB8E155A746A9C47 (voto_aviso_id), PRIMARY KEY(administrador_id, voto_aviso_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE administrador_aviso (administrador_id INT NOT NULL, aviso_id INT NOT NULL, INDEX IDX_C0CFA62B48DFEBB7 (administrador_id), INDEX IDX_C0CFA62B9D17A5A7 (aviso_id), PRIMARY KEY(administrador_id, aviso_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE basico_alerta_informacion (basico_id INT NOT NULL, alerta_informacion_id INT NOT NULL, INDEX IDX_321A8EB5E717F67 (basico_id), INDEX IDX_321A8EBDFC93304 (alerta_informacion_id), PRIMARY KEY(basico_id, alerta_informacion_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE aviso (id INT AUTO_INCREMENT NOT NULL, latitud DOUBLE PRECISION NOT NULL, longitud DOUBLE PRECISION NOT NULL, tipo VARCHAR(255) NOT NULL, ip_creador VARCHAR(255) NOT NULL, fecha DATETIME NOT NULL, comentario_creador VARCHAR(255) DEFAULT NULL, imagen LONGBLOB DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE alerta_informacion (id INT AUTO_INCREMENT NOT NULL, radio_km DOUBLE PRECISION NOT NULL, latitud DOUBLE PRECISION NOT NULL, longitud DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE alerta_informacion_aviso (alerta_informacion_id INT NOT NULL, aviso_id INT NOT NULL, INDEX IDX_BAE21CB3DFC93304 (alerta_informacion_id), INDEX IDX_BAE21CB39D17A5A7 (aviso_id), PRIMARY KEY(alerta_informacion_id, aviso_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE voto_aviso ADD CONSTRAINT FK_4724CD769D17A5A7 FOREIGN KEY (aviso_id) REFERENCES aviso (id)');
        $this->addSql('ALTER TABLE administrador_voto_aviso ADD CONSTRAINT FK_BB8E155A48DFEBB7 FOREIGN KEY (administrador_id) REFERENCES usuario (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrador_voto_aviso ADD CONSTRAINT FK_BB8E155A746A9C47 FOREIGN KEY (voto_aviso_id) REFERENCES voto_aviso (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrador_aviso ADD CONSTRAINT FK_C0CFA62B48DFEBB7 FOREIGN KEY (administrador_id) REFERENCES usuario (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administrador_aviso ADD CONSTRAINT FK_C0CFA62B9D17A5A7 FOREIGN KEY (aviso_id) REFERENCES aviso (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE basico_alerta_informacion ADD CONSTRAINT FK_321A8EB5E717F67 FOREIGN KEY (basico_id) REFERENCES usuario (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE basico_alerta_informacion ADD CONSTRAINT FK_321A8EBDFC93304 FOREIGN KEY (alerta_informacion_id) REFERENCES alerta_informacion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE alerta_informacion_aviso ADD CONSTRAINT FK_BAE21CB3DFC93304 FOREIGN KEY (alerta_informacion_id) REFERENCES alerta_informacion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE alerta_informacion_aviso ADD CONSTRAINT FK_BAE21CB39D17A5A7 FOREIGN KEY (aviso_id) REFERENCES aviso (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE administrador_voto_aviso DROP FOREIGN KEY FK_BB8E155A746A9C47');
        $this->addSql('ALTER TABLE administrador_voto_aviso DROP FOREIGN KEY FK_BB8E155A48DFEBB7');
        $this->addSql('ALTER TABLE administrador_aviso DROP FOREIGN KEY FK_C0CFA62B48DFEBB7');
        $this->addSql('ALTER TABLE basico_alerta_informacion DROP FOREIGN KEY FK_321A8EB5E717F67');
        $this->addSql('ALTER TABLE voto_aviso DROP FOREIGN KEY FK_4724CD769D17A5A7');
        $this->addSql('ALTER TABLE administrador_aviso DROP FOREIGN KEY FK_C0CFA62B9D17A5A7');
        $this->addSql('ALTER TABLE alerta_informacion_aviso DROP FOREIGN KEY FK_BAE21CB39D17A5A7');
        $this->addSql('ALTER TABLE basico_alerta_informacion DROP FOREIGN KEY FK_321A8EBDFC93304');
        $this->addSql('ALTER TABLE alerta_informacion_aviso DROP FOREIGN KEY FK_BAE21CB3DFC93304');
        $this->addSql('DROP TABLE voto_aviso');
        $this->addSql('DROP TABLE usuario');
        $this->addSql('DROP TABLE administrador_voto_aviso');
        $this->addSql('DROP TABLE administrador_aviso');
        $this->addSql('DROP TABLE basico_alerta_informacion');
        $this->addSql('DROP TABLE aviso');
        $this->addSql('DROP TABLE alerta_informacion');
        $this->addSql('DROP TABLE alerta_informacion_aviso');
    }
}
